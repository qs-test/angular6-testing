import { AppPage } from './app.po';

describe('workspace-project App', () => { // Agrupa los tests.
  let page: AppPage;

  beforeEach(() => { // Antes de cada Test
    page = new AppPage();
  });

  // probar funcion
  it("should display welcome message", () => {
    page.navigateTo("/calc");
    const text = page.getText("app-root h1");
    expect(text).toEqual("Welcome to testing!");
  });

  it("Result = 25", async () => { // resumen del resultado
    // beforeEach
    page.navigateTo("/calc");

    // await page.getElement("app-calculator #num1").clear();
    page.getElement("app-calculator #num1").sendKeys(15);
    page.getElement("app-calculator #num2").sendKeys(10);
    await page.getElement("app-calculator #button").click();

    const result = page.getElement("app-calculator #result").getAttribute("value");
    expect(result).toBe('25');
    // afterEach
  });

});
