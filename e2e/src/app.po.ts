import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(url: string) {
    return browser.get(url); // retorna una url
  }

  getText(s: string) {
    return element(by.css(s)).getText();
  }

  getElement(s: string) {
    return element(by.css(s));
  }
}
