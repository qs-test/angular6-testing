import { Component, OnInit, ViewChild } from "@angular/core";

@Component({
  selector: "app-calculator",
  templateUrl: "./calculator.component.html",
  styleUrls: ["./calculator.component.css"],
})
export class CalculatorComponent implements OnInit {

  @ViewChild("result") result: any;
  constructor() {}

  ngOnInit() {}

  sum(num1: number, num2: number): number {
    return Number(num1) + Number(num2);
  }

  printSum(num1: number, num2: number): any {
    this.result.nativeElement.value = this.sum(num1, num2);
  }
}
