import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculatorComponent } from './calculator.component';

describe('CalculatorComponent', () => { // Agrupa los tests.
  let component: CalculatorComponent;
  let fixture: ComponentFixture<CalculatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalculatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  afterEach(async(() => {
    (<HTMLInputElement>document.getElementById("num1")).value = "0";
    (<HTMLInputElement>document.getElementById("num2")).value = "0";
    document.getElementById("button").click();
  }));

  // probar funcion
  it('Result = 9', async(() => { // resume el resultado
    expect(component.sum(3, 6)).toEqual(9); // lo que estamos esperando, la suma debe dar 9.
  }));

  // probar sin funcion
  it('Result = 25', async(() => {
    // beforeEach
    (<HTMLInputElement>document.getElementById('num1')).value = '15';
    (<HTMLInputElement>document.getElementById('num2')).value = '10';
    document.getElementById('button').click();
    const result = (<HTMLInputElement>document.getElementById('result')).value;
    expect(result).toBe('25'); // Igual a
    // expect((<HTMLInputElement>document.getElementById('result')).value).not.toBe('25'); // Negar igualdad
    // expect((<HTMLInputElement>document.getElementById('result')).value).toEqual('25'); // Estrictamente igual a
    // afterEach
  }));
});
