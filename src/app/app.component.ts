import { Component, ViewChild } from "@angular/core";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('result') result: any;
  title = 'testing';

  ngOnInit() {}

  sum(num1: number, num2: number): number {
    return Number(num1) + Number(num2)
  }

  printSum(num1: number, num2: number): any {
    this.result.nativeElement.value = this.sum(num1, num2);
  }
}
